<?php

const STATUS_DONE = 1;
const LIMIT = 10;

const PENDING = 0;
const DELIVERY = 1;
const FINISH = 2;
const CANCEL = 3;