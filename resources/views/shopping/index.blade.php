@extends('layouts.app')
@section('content')
<div class="our-product-area new-product">
    <div class="container">
        <div class="area-title">
            <h2>Giỏ hàng của bạn</h2>
        </div>
        <table class="table">
			    <thead>
			      <tr>
			        <th>STT</th>
			        <th>Tên SP</th>
			        <th>Hình ảnh</th>
			        <th>Giá</th>
			        <th>Số lượng</th>
			        <th>Thành tiền</th>
			        <th>Thao tác</th>
			      </tr>
			    </thead>
			    <tbody>
			    	<?php $i=1 ?>
			    	@foreach($products as $key => $product)
			      <tr>
				      	<td>#{{$i}}</td>
				        <td><a href="">{{$product->name}}</a></td>
				        <td>
				        	<img style="width: 80px;height: 80px;" src="{{asset("")}}/{{pare_url_file($product->options->avatar)}}">
				        </td>
				        <td class="price">{{number_format($product->price,0,',','.')}} đ</td>
				        {{-- <td>{{$product->qty}}</td> --}}
				        <td style="width: 13%;">
				        <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="quantity-left-minus btn btn-danger btn-number" data-mins="{{$product->rowId}}" id="minusQuanlity">
                                          <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>
                                    <input type="text" id="quantity" name="quantity" class="form-control input-number" value="{{$product->qty}}" min="1" max="100">
                                    <span class="input-group-btn">
                                        <button type="button" class="quantity-right-plus btn btn-success btn-number" data-add="{{$product->rowId}}" id="add_quanlity">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div>
                        </td>
				        {{-- <td>{{$product->qty}}</td> --}}
				        <td class="price_total">{{number_format($product->qty * $product->price,0,',','.')}} đ</td>
				        <td>
			                <a class="badge badge-danger" style="padding:5px 10px;border:1px solid #eee;" href="{{route('delete.shopping.cart',$key)}}"><i class="fas fa-trash-alt"></i> Xóa</a>
				        </td>
			      </tr>
			      <?php $i++ ?>
			      	@endforeach
			    </tbody>
			  </table>
			  <h4 class="pull-right">Tổng tiền thanh toán: {{\Cart::subtotal()}} <button  class="btn-success btn" id="disabled_btn"><a href="{{route('get.form.pay')}}" style="text-decoration: none; color: white !important;">Thanh toán</a></button></h4>

       </div>
  </div>
 
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@endsection

@section('js')
	<script>
		
$(document).ready(function(){

// var quantitiy=0;
//    $('.quantity-right-plus').click(function(e){
        
//         // Stop acting like a button
//         e.preventDefault();
//         // Get the field name
//         var quantity = parseInt($('#quantity').val());
        
//         // If is not undefined
            
//             $('#quantity').val(quantity + 1);
//             {{-- $('#quantity').val({{route('add.shopping.cart',$product->id)}}); --}}

          
//             // Increment
        
//     });

//      $('.quantity-left-minus').click(function(e){
//         // Stop acting like a button
//         e.preventDefault();
//         // Get the field name
//         var quantity = parseInt($('#quantity').val());
        
//         // If is not undefined
      
//             // Increment
//             if(quantity>0){
//             $('#quantity').val(quantity - 1);
//             }
//     });
  
     $('#add_quanlity').click(function(){
        let id = $(this).attr('data-add');
        // $("#ajaxStart").attr("disabled", false);
        // var soluong = $('#quantity').val();
        // console.log(soluong);
        let url = '{{route('cart.update', ':id')}}';
        url = url.replace(':id', id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (response) {
                     if (response == 2) {
                        alert('Số lượng sản phẩm k đủ !');

                     }
                    location.reload();
                    
                },
                error: function (response) {
                    console.log("errors");
                    console.log(response);
                }
            });
     });

      $('#minusQuanlity').click(function(){
        let id = $(this).attr('data-mins');
        // alert(id);
        let url = '{{route('cart.minsCart', ':id')}}';
        url = url.replace(':id', id);
            $.ajax({
                url: url,
                method: 'get',
                success: function (response) {
                    location.reload();
                },
                error: function (response) {
                    console.log("errors");
                    console.log(response);
                }
            });
     });

});
	</script>
@endsection