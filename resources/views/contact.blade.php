@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="container-inner">
							<ul>
								<li class="home">
									<a href="{{route('home')}}">Trang chủ</a>
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								<li class="category3"><span>Liên hệ</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

<div class="main-contact-area">
			<div class="container">
				<div class="row">
						
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12	">
						<div class="contact-us-area">
							<!-- google-map-area start -->
							<div class="google-map-area">
								<!--  Map Section -->
								<div id="contacts" class="map-area">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3131.4521349544784!2d106.12399395282905!3d21.034880210288758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a04e667cc5dd%3A0x54ba6e39a4b67d77!2zQ8O0bmcgVHkgVE5ISCBYw6J5IEThu7FuZyBWw6AgVGjGsMahbmcgTeG6oWkgUGjDumMgVGjDoG5o!5e0!3m2!1svi!2s!4v1575866042999!5m2!1svi!2s" width="1170" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
								</div>

							</div>
							<!-- google-map-area end -->
							<!-- contact us form start -->
							<div class="contact-us-form">
								<div class="sec-heading-area">
									<h2>Liên hệ với chúng tôi</h2>
								</div>
								<div class="contact-form">
									<span class="legend">mời bạn điền thông tin liên hệ</span>
									<form action="" method="post">
										@csrf
										<div class="form-top">
											
											<div class="form-group col-sm-6 col-md-6 col-lg-5">
												<label>Họ tên <sup>*</sup></label>
												<input type="text" name="c_name" class="form-control" required>
											</div>
												{{-- <div class="form-group col-sm-6 col-md-6 col-lg-5">
													<label>Số ĐT <sup>*</sup></label>
													<input type="text" name="c_phone" class="form-control">
												</div>	 --}}
											<div class="form-group col-sm-6 col-md-6 col-lg-5">
												<label>Email <sup>*</sup></label>
												<input type="email" name="c_email" class="form-control" required>
											</div>
											<div class="form-group col-sm-6 col-md-6 col-lg-5">
												<label>Tiêu đề <sup>*</sup></label>
												<input type="text" name="c_title" class="form-control" required>
											</div>											
											
											<div class="form-group col-sm-12 col-md-12 col-lg-10">
												<label>Nội dung <sup>*</sup></label>
												<textarea class="yourmessage" name="c_content" required></textarea>
											</div>												
										</div>
										<div class="submit-form form-group col-sm-12 submit-review">
											<p><sup>*</sup> Required Fields</p>
											<button type="submit" class="add-tag-btn">Gửi thông tin</button>										</div>
									</form>
								</div>
							</div>
							<!-- contact us form end -->
						</div>					
					</div>
				</div>
			</div>	
		</div>

@stop