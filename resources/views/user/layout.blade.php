<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tổng quan User</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <base href="{{asset('')}}">
    <link rel="stylesheet" href="theme_admin/assets/css/normalize.css">
    <link rel="stylesheet" href="theme_admin/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="theme_admin/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="theme_admin/assets/css/themify-icons.css">
    <link rel="stylesheet" href="theme_admin/assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="theme_admin/assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="theme_admin/assets/scss/style.css">
    <link rel="stylesheet" href="theme_admin/assets/css/writecss.css">
    <link href="theme_admin/assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <script src="https://kit.fontawesome.com/c60f6dddbd.js" crossorigin="anonymous"></script>
    

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    

</head>
<body>


        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{route('home')}}"><img src="img/logoxq1.png" alt="Logo"></a>

                <a class="navbar-brand hidden" href="{{route('home')}}"><img src="img/logoxq1.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="{{\Request::route()->getName()=='user.dashboard' ? 'active' : ''}}">
                        <a href="{{route('user.dashboard')}}"> <i class="menu-icon fa fa-dashboard"></i>Trang tổng quan </a>
                    </li>
                  
                    <li class="menu-item-has-children {{\Request::route()->getName()=='user.update.info' ? 'active' : ''}}">
                        <a href="{{route('user.update.info')}}" class="dropdown-toggle" > <i class="menu-icon fa fa-list"></i>Cập nhật thông tin</a>
                    </li>

                     <li class="menu-item-has-children {{\Request::route()->getName()=='user.update.password' ? 'active' : ''}}">
                        <a href="{{route('user.update.password')}}" class="dropdown-toggle" > <i class="menu-icon fa fa-list"></i>Cập nhật mật khẩu</a>
                    </li>
                    </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="theme_admin/assets/images/admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                           
                                <a class="nav-link" href="#"><i class="fa fa- user"></i>{{get_data_user('web','name')}}</a>
                                <a class="nav-link" href="{{route('get.login')}}" title="Đăng xuất"><i class="fa fa-power -off"></i>Logout</a>
                          
                        </div>
                    </div>

                   
                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->

     
        <div class="content mt-3">
            @if(count($errors) > 0)
               <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-danger"> @foreach($errors->all() as $err)
                  {{$err}}<br>
                  @endforeach</span> 
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               @endif
               @if (session('thongbao'))
               <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               @endif
         @yield('content')   
    
        </div> <!-- .content -->
    </div><!-- /#right-panel -->



    <!-- Right Panel -->


    <script src="theme_admin/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="theme_admin/assets/js/popper.min.js"></script>
   {{--  <script src="theme_admin/assets/js/toastr.min.js"></script> --}}
    <script src="theme_admin/assets/js/plugins.js"></script>
    <script src="theme_admin/assets/js/main.js"></script>


    <script src="theme_admin/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/datatables-init.js"></script>

    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckfinder/ckfinder.js"></script>
   
       

    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>
    <script>
        function readURL(input) {
              if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('#out_img').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
              }
            }
            $("#input_img").change(function() {
              readURL(this);
            });
    </script>
    
    @yield('js')
    
  
</body>
</html>
