@extends('user.layout')
@section('content')
<div class="breadcrumbs">
   <div class="col-sm-8">
      <div class="page-header float-left">
         <div class="page-title menu">
            
            <h1 style="color: blue;">Cập nhật mật khẩu</h1>
         
         </div>
      </div>
   </div>
 </div>
<div class="card">
   <div class="card-header">

      <strong>Cập nhật</strong> 
   </div>
<div class="card-body card-block">
      <form action="" method="post">
      @csrf
      <div class="form-group" style="position: relative;">
        <label for="email">Mật khẩu cũ</label>
        <input type="password" class="form-control" id="email"  name="password_old" placeholder="******" value="">
        <a style="position: absolute;top: 54%;right: 10px;" href="javascrip:;void(0)"><i class="fa fa-eye"></i></a>
      
       @if($errors->has('password_old'))
            <span class="error-text">
            {{$errors->first('password_old')}}
            </span>
            @endif

      </div>
      <div class="form-group" style="position: relative;">
        <label for="email">Mật khẩu mới</label>
        <input type="password" class="form-control" id="email"  name="password" placeholder="******" value="">
        <a style="position: absolute;top: 54%;right: 10px;" href="javascrip:;void(0)"><i class="fa fa-eye"></i></a>

         @if($errors->has('password'))
            <span class="error-text">
            {{$errors->first('password')}}
            </span>
            @endif
      </div>
      <div class="form-group" style="position: relative;">
        <label for="email">Xác nhận lại mật khẩu:</label>
        <input type="password" class="form-control" id="email"  name="password_confirm" placeholder="******" value="">
        <a style="position: absolute;top: 54%;right: 10px;" href="javascrip:;void(0)"><i class="fa fa-eye"></i></a>

         @if($errors->has('password_confirm'))
            <span class="error-text">
            {{$errors->first('password_confirm')}}
            </span>
            @endif
      </div>
      <button type="submit" class="btn btn-info"><i class="fa fa-save"> Cập nhật</i></button>
    </form>
   </div>
 </div>
{{-- <div class="row">
  <div class="col-sm-12">
   
    </div>
</div> --}}

@stop

@section('js')
  <script>
    $(function(){
      $('.form-group a').click(function(){
        let $this = $(this);
        if($this.hasClass('active'))
        {
          $this.parents('.form-group').find('input').attr('type','password')
          $this.removeClass('active');
        }else
        {
          $this.parents('.form-group').find('input').attr('type','text')
          $this.addClass('active');
        }
      });
    });
  </script>
@stop