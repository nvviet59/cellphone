@extends('user.layout')
@section('content')
<div class="breadcrumbs">
   <div class="col-sm-8">
      <div class="page-header float-left">
         <div class="page-title menu">
            
            <h1 style="color: blue;">Cập nhật thông tin</h1>
         
         </div>
      </div>
   </div>
 </div>
<div class="card">
   <div class="card-header">

      <strong>Cập nhật</strong> 
   </div>
<div class="card-body card-block">
      <form action="" method="post">
      @csrf
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" disabled="" name="email" placeholder="Email" value="{{$user->email}}">
      </div>
      <div class="form-group">
        <label for="pwd">Họ tên</label>
        <input type="text" class="form-control" id="pwd" name="name" placeholder="Họ tên" value="{{$user->name}}">
      </div>
     <div class="form-group">
        <label for="pwd">Số điện thoại</label>
        <input type="number" class="form-control" id="pwd" placeholder="Điện thoại" name="phone" value="{{$user->phone}}">
      </div>
      <div class="form-group">
        <label for="pwd">Địa chỉ</label>
        <input type="text" class="form-control" id="pwd" placeholder="Địa chỉ" name="address" value="{{$user->address}}">
      </div>
      <div class="form-group">
        <label for="pwd">Giới thiệu</label>
        <textarea name="about" cols="30" rows="5" class="form-control" placeholder="Mô tả" value="" >{{$user->about}}</textarea>
      </div>
      <button type="submit" class="btn btn-info"><i class="fa fa-save"> Cập nhật</i></button>
    </form>
   </div>
 </div>
{{-- <div class="row">
  <div class="col-sm-12">
   
    </div>
</div> --}}

@stop