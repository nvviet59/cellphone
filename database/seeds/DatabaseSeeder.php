<?php

use Illuminate\Database\Seeder;
// use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \DB::table('admins')->insert([
        	'name' =>'Nguyễn Việt',
        	'email' =>'nvviet59@gmail.com',
        	'password' =>bcrypt('123')
        ]);
    }
}
