 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Giá</th>
                        <th>Giảm giá</th>
                        <th>Số lượng còn</th>
                        <th>Số lượng đã bán</th>
                        <th>Loại sản phẩm</th>
                       {{--  <th>Hình ảnh</th>
                        <th>Trạng thái</th>
                        <th>Nổi bật</th>
                        <th>Thao tác</th> --}}
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($products))
                     @foreach($products as $key => $product)
                     
                     <?php 
                        $age =0;
                        if($product->pro_total_rating)
                        {
                           $age = round($product->pro_total_number/$product->pro_total_rating,2);
                        }
                     ?>

                     <?php 
                        $price = $product->pro_price;
                        
                           if($product->pro_sale)
                           {
                               $price = $price *(100 - $product->pro_sale)/100;
                        }
                         ?>
                     <tr>
                        <td>{{$key+1}}</td>
                        <td>
                           {{$product -> pro_name}}
                         {{--   <ul style="padding-left: 17px;">
                              <li><span><i class="fas fa-dollar-sign" style="padding-right: 3px;"></i></span><span>{{$product -> pro_price}}(đ)</span></li>
                              <li><span><i class="fas fa-dollar-sign" style="padding-right: 3px;"></i></span><span>{{$product -> pro_sale}}(%)</span></li>

                              <li><span>Đánh giá:</span>
                                 <span class="rating">
                                    @for($i=1;$i<=5;$i++)
                                          <i class="fa fa-star {{$i <= $age ?'active':''}}" style="color: #999;"></i>
                                    @endfor
                                 </span>
                              </li>
                              <li><span>Số lượng:</span><span>{{$product->pro_number}}</span></li>
                           </ul> --}}
                        </td>
                        <td>{{number_format($product->pro_price,0,',','.')}}(đ)</td>
                        <td>{{$product -> pro_sale}}(%) = {{number_format($price,0,',','.')}}đ</td>
                        <td>{{$product->pro_number}}</td>
                        <td>{{$product->pro_pay}}</td>
                        <td>{{ isset($product->category->c_name) ? $product->category->c_name : '[N\A]'}}</td>
                       {{--  <td>
                           <img src="{{asset("")}}/{{ pare_url_file($product->pro_avatar)}}" alt="ảnh" class="img img-reponsive" style="height: 100px;width: 100px;">
                        </td>
                        <td>
                           <a href="{{route('admin.get.action.product',['active',$product->id])}}" class="badge {{$product->getStatus($product->pro_active)['class']}}">{{$product ->getStatus($product->pro_active)['name']}}</a>
                        </td>
                        <td>
                           <a href="{{route('admin.get.action.product',['hot',$product->id])}}" class="badge {{$product->getHot($product->pro_hot)['class']}}">{{$product ->getHot($product->pro_hot)['name']}}</a>
                        </td> --}}
                        {{-- <td>
                           <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.product',$product->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a class="badge badge-danger" onclick="return confirm('Bạn có muốn xóa tin này?')" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.action.product',['delete',$product->id])}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td> --}}
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>

