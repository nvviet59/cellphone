@extends('admin::layouts.master')
@section('content')

<style>
   .rating .active{color:#ff9705 !important;}
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Sản phẩm</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Quản lý sản phẩm <a href="{{route('admin.get.create.product')}}" class="pull-right badge badge-warning"><i class="fas fa-plus-circle"></i> Thêm mới</a></strong>

                        </div>
                        <a href="{{route("export")}}" class="btn btn-danger">Export Excel</a>
                        <div class="card-body">
<form action="/admin/product/delete" method="post">
@csrf
                 {{-- @include("admin::product.table",$products) --}}
                  <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Stt</th>
                        <th>Tên sản phẩm</th>
                        <th>Loại sản phẩm</th>
                        <th>Hình ảnh</th>
                        <th>Trạng thái</th>
                        <th>Nổi bật</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($products))
                     @foreach($products as $key => $product)
                     
                     <?php 
                        $age =0;
                        if($product->pro_total_rating)
                        {
                           $age = round($product->pro_total_number/$product->pro_total_rating,2);
                        }
                     ?>

                     <tr>
                        <td><input type="CheckBox" name="delid[]" value="{{$product->id}}"></td>
                        <td>{{$key+1}}</td>
                        <td>
                           {{$product -> pro_name}}
                           <ul style="padding-left: 17px;">
                              <li><span><i class="fas fa-dollar-sign" style="padding-right: 3px;"></i></span><span>{{number_format($product->pro_price,0,',','.')}}(đ)</span></li>
                              <li><span><i class="fas fa-dollar-sign" style="padding-right: 3px;"></i></span><span>{{$product -> pro_sale}}(%)</span></li>

                              <li><span>Đánh giá:</span>
                                 <span class="rating">
                                    @for($i=1;$i<=5;$i++)
                                          <i class="fa fa-star {{$i <= $age ?'active':''}}" style="color: #999;"></i>
                                    @endfor
                                 </span>
                              </li>

                              <li><span>Số lượng:</span><span>{{$product->pro_number}}</span></li>
                           </ul>
                        </td>
                        <td>{{ isset($product->category->c_name) ? $product->category->c_name : '[N\A]'}}</td>
                        <td>
                           <img src="{{asset("")}}/{{ pare_url_file($product->pro_avatar)}}" alt="ảnh" class="img img-reponsive" style="height: 100px;width: 100px;">
                        </td>
                        <td>
                           <a href="{{route('admin.get.action.product',['active',$product->id])}}" class="badge {{$product->getStatus($product->pro_active)['class']}}">{{$product ->getStatus($product->pro_active)['name']}}</a>
                        </td>
                        <td>
                           <a href="{{route('admin.get.action.product',['hot',$product->id])}}" class="badge {{$product->getHot($product->pro_hot)['class']}}">{{$product ->getHot($product->pro_hot)['name']}}</a>
                        </td>
                        <td>
                           <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.product',$product->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a class="badge badge-danger" onclick="return confirm('Bạn có muốn xóa tin này?')" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.action.product',['delete',$product->id])}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
               <button type="submit" class="btn-danger">Delete Selected</button>
</form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .animated -->
@endsection

