@extends('admin::layouts.master')
@section('content')
{{-- <div class="card-header"> --}}
@if (session('thongbao'))
<div class="alert  alert-success alert-dismissible fade show" style="width:250px;float: right;" role="alert">
   <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
   <span aria-hidden="true">×</span>
   </button>
</div>
@endif
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Sản phẩm</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.get.list.product')}}">Danh sách</a></li>
              <li class="breadcrumb-item">Thêm mới</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Thêm mới</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            {{-- @include("admin::product.form") --}}
            <style>
   .error-text{
   color:red;
   }
   #del_img_demo{
     position: relative;
     top: -33px; 
   }
</style>
{{-- @if (session('thongbao'))
<div class="alert  alert-success alert-dismissible fade show" style="width:250px;float: right;" role="alert">
   <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
   <span aria-hidden="true">×</span>
   </button>
</div>
@endif --}}
<form action="" method="post" enctype="multipart/form-data">
   @csrf
   <div class="row">
      <div class="col-sm-5">
         <div class="form-group">
            <label for="pro_name">Tên sản phẩm(*)</label>
            <input type="text" class="form-control" required="" placeholder="Tên sản phẩm" value="{{ old('pro_name',isset($product->pro_name) ? $product->pro_name : '')}}" name="pro_name">
            @if($errors->has('pro_name'))
            <span class="error-text">
            {{$errors->first('pro_name')}}
            </span>
            @endif
         </div>
         <div class="form-group">
            <label for="name">Loại sản phẩm(*):</label>
            <select name="pro_category_id" id="" class="form-control">
               <option value="">--Chọn loại sản phẩm--</option>
               @if(isset($categories))
               @foreach($categories as $category)
               <option value="{{$category->id}}"{{old('pro_category_id',isset($product->pro_category_id) ? $product->pro_category_id : '') == $category->id ? "selected=='selected'" : "" }}>{{$category->c_name}}</option>
               @endforeach
               @endif
            </select>
            @if($errors->has('pro_category_id'))
            <span class="error-text">
            {{$errors->first('pro_category_id')}}
            </span>
            @endif
         </div>
         <div class="form-group">
            <label for="name">Mô tả:</label>
            <textarea name="pro_description" class="form-control" id="" cols="30" rows="2" placeholder="Mô tả ngắn sản phẩm" value="">{{ old('pro_description',isset($product->pro_description) ? $product->pro_description : '')}}</textarea>
            @if($errors->has('pro_description'))
            <span class="error-text">
            {{$errors->first('pro_description')}}
            </span>
            @endif
         </div>
         <div class="form-group">
            <label for="name">Nội dung(*):</label>
            <textarea name="pro_content" class="form-control" id="pro_content" cols="30" rows="3" placeholder="Nội dung" value="">{{ old('pro_content',isset($product->pro_content) ? $product->pro_content : '')}}</textarea>
            @if($errors->has('pro_content'))
            <span class="error-text">
            {{$errors->first('pro_content')}}
            </span>
            @endif
         </div>
         {{--  
         <div class="form-group">
            <label for="name">Thông số:</label>
            @foreach($para as $pa)
            <textarea name="para_name" class="form-control"  id="a_content" cols="30" rows="3" placeholder="Nội dung" value="{{$pa->id}}">{{old('para_name',isset($pa->para_name) ? $pa->para_name:'')}}</textarea>
            @endforeach
            @if($errors->has('para_name'))
            <span class="error-text">
            {{$errors->first('para_name')}}
            </span>
            @endif
         </div>
         --}}
         {{--  
         <div class="form-group">
            <label for="name">Meta title:</label>
            <input type="text" class="form-control" placeholder="Meta title" value="{{ old('pro_title_seo',isset($product->pro_title_seo) ? $product->pro_title_seo : '')}}" name="pro_title_seo">
         </div>
         <div class="form-group">
            <label for="name">Meta desciption:</label>
            <input type="text" class="form-control" placeholder="Meta desciption" value="{{ old('pro_description_seo',isset($product->pro_description_seo) ? $product->pro_description_seo : '')}}" name="pro_description_seo">
         </div>
         --}}
      </div>
      <div class="col-sm-3">
         
         <div class="form-group">
            <label for="pro_price">Giá sản phẩm(*):</label>
            <input type="number" placeholder="Giá sản phẩm" class="form-control" name="pro_price" value="{{old('pro_price',isset($product->pro_price) ? $product->pro_price : '')}}">
            @if($errors->has('pro_price'))
            <span class="error-text">
            {{$errors->first('pro_price')}}
            </span>
            @endif
         </div>
         <div class="form-group">
            <label for="pro_sale">% Khuyến mãi:</label>
            <input type="number" placeholder="% giảm giá" class="form-control" name="pro_sale" value="{{old('pro_sale',isset($product->pro_sale) ? $product->pro_sale : '0')}}">
         </div>
         <div class="form-group">
            <label for="pro_sale">Số lượng sản phẩm:</label>
            <input type="number" placeholder="10" class="form-control" name="pro_number" value="{{old('pro_number',isset($product->pro_number) ? $product->pro_number : '0')}}">
         </div>

      @if(isset($product->pro_avatar))
         <div class="form-group">
            <img height="100px;" src="{{asset("")}}/{{ pare_url_file($product->pro_avatar)}}" id="out_img">
         </div>
         <div class="form-group">
            <label for="avatar">Ảnh chính:</label>
            <input type="file" name="avatar" id="input_img" class="form-control">
         </div>
      @else
         <div class="form-group">
            <img height="100px;" src="{{asset('image/default.jpg')}}" id="out_img">
         </div>
         <div class="form-group">
            <label for="avatar">Ảnh chính:</label>
            <input type="file" name="avatar" id="input_img" class="form-control">
         </div>
      @endif

<!--Thêm ảnh phụ-->
      {{-- @if(isset($product_img))

         @for($i=1;$i<=3;$i++)
            @foreach($product_img as $pro)
            <div class="form-group">
               <label for="avatar">Ảnh phụ {!!$i!!}:</label>
               <img src="uploads/detail/{{$pro->image}}">
               <input type="file" name="imageDetail[]" src="uploads/detail/{{$pro->image}}" class="form-control">
            </div>
            @endforeach
         @endfor

      @else --}}


     
       @for($i=1;$i<=3;$i++)
            <div class="form-group">
               <label for="avatar">Ảnh phụ {!!$i!!}:</label>
               <input type="file" name="imageDetail[]" class="form-control">
            </div>
         @endfor


<!--Thêm ảnh phụ-->

          <div class="form-group">
                     <label>Nổi bật: </label>
                     <label class="radio-inline">
                     <input name="pro_hot" value="1" type="radio">On
                     </label>
                     <label class="radio-inline">
                     <input name="pro_hot" value="0" checked="" type="radio">Off
                     </label>
                  </div>
      </div>
      <div class="col-sm-4">
         <div class="card card-info">
            <div class="card-header">
               <h3 class="card-title">Thông số kỹ thuật</h3>
            </div>
            <div class="card-body">
               <div class="form-group">
                  
                  <div class="row div-test">
                     @if(isset($product))
                     <div class="row">
                     <div class="col-md-5">
                        <label>Tên thông số()</label>
                        @foreach(json_decode($product->key_param) as $key_param)
                           <input style="margin-top: 5px;" type="text" class="form-control" name="key_param[]" value="{{$key_param}}" class="tesst">
                        @endforeach
                        </div>
                        <div class="col-md-5">
                        <label>Nội dung thông số()</label>
                        @foreach(json_decode($product->value_param) as $value_param)
                           <input style="margin-top: 5px;" type="text" class="form-control" name="value_param[]" value="{{$value_param}}" class="tesst">
                        @endforeach
                        </div>
                     </div>
                  @else
                     <div class="row">
                        <div class="col-md-5">
                           <label>Tên thông số()</label>
                           <input type="text" class="form-control" name="key_param[]" value="Camera chính" class="tesst">
                        </div>
                        <div class="col-md-5">
                           <label>Nội dung thông số()</label>
                           <input type="text" class="form-control" name="value_param[]" class="tesst">
                        </div>
                     </div>
                     <div class="row" style="margin-top: 5px;">
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="key_param[]" value="Camera phụ" class="tesst">
                        </div>
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="value_param[]" class="tesst">
                        </div>
                     </div>
                     <div class="row" style="margin-top: 5px;">
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="key_param[]" value="Màn hình" class="tesst">
                        </div>
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="value_param[]" class="tesst">
                        </div>
                     </div>
                     <div class="row" style="margin-top: 5px;">
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="key_param[]" value="Bộ nhớ trong" class="tesst">
                        </div>
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="value_param[]" class="tesst">
                        </div>
                     </div>
                     <div class="row" style="margin-top: 5px;">
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="key_param[]" value="Ram" class="tesst">
                        </div>
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="value_param[]" class="tesst">
                        </div>
                     </div>
                     <div class="row" style="margin-top: 5px;">
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="key_param[]" value="Dung lượng pin" class="tesst">
                        </div>
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="value_param[]" class="tesst">
                        </div>
                     </div>
                     <div class="row" style="margin-top: 5px;">
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="key_param[]" value="Quay phim" class="tesst">
                        </div>
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="value_param[]" class="tesst">
                        </div>
                     </div>
                     <div class="row" style="margin-top: 5px;">
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="key_param[]" value="Hệ điều hành" class="tesst">
                        </div>
                        <div class="col-md-5">
                           <input type="text" class="form-control" name="value_param[]" class="tesst">
                        </div>
                     </div>
                  </div>
                  @endif
               </div>
               <button type="button"  class="btn btn-info btn-sm" id="btn1"><i class="fas fa-plus"></i></button>
            </div>
            <!-- /input-group -->
         </div>
         <!-- /.card-body -->
      </div>
   </div>
   <button type="submit" class="btn btn-primary btn-sm">
   <i class="fa fa-dot-circle-o"></i> Lưu thông tin
   </button>
</form>


</div>

@section('js')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
   
   $(document).ready(function(){
      $(".del_img_demo").on('click',function(){

         var value = $(this).attr('data-id');
        // alert(value);
         $.ajax({

            url:'xoakhongduoc/'+value,
            method:'get',
            success:function(){
             // console.log(kq);
             location.reload();
            },
            error:function(){
               alert('lỗi');
            },
         });
      });
   });
</script>


<script>
$(document).ready(function(){
  var i = 1;
  $("#btn1").click(function(){
    
    i++;
    // alert(count);
    $(".div-test").append('<div class="row" id="row'+i+'"><div class="col-md-5 xoa" style="margin-top:5px;"><input type="text" class="form-control" name="key_param[]" class="tesst"></div><div class="col-md-5"  style="margin-top:5px;"><input type="text" class="form-control" name="value_param[]" class="tesst"></div><div class="col-md-2" style="margin-top:5px;"><button type="button" class="btn btn-danger btn-sm btn_remove" name="remove" id="'+i+'"><i class="fas fa-trash-alt"></i></button></div></div>');
  });
   $(document).on('click','.btn_remove',function(){
        // alert('ok');

      var button_id = $(this).attr("id");
      $("#row"+button_id+"").remove();

      // alert(button_id);
  });
});

</script>
<script>
   CKEDITOR.replace( 'pro_content', {
       language:'vi',
       filebrowserBrowseUrl: 'http://localhost/Web_Cellphone/public/ckfinder/ckfinder.html',
       filebrowserUploadUrl: 'http://localhost/Web_Cellphone/public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
       filebrowserWindowWidth: '1000',
       filebrowserWindowHeight: '700'
   } );
</script>
{{-- <script>
   CKEDITOR.replace( 'a_content', {
       language:'vi',
       filebrowserBrowseUrl: 'http://localhost/Web_Cellphone/public/ckfinder/ckfinder.html',
       filebrowserUploadUrl: 'http://localhost/Web_Cellphone/public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
       filebrowserWindowWidth: '1000',
       filebrowserWindowHeight: '700'
   } );
</script> --}}
@endsection
      
   </div>
  </div>
</div>
</div>
</section>
@endsection