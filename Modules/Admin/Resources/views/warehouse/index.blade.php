@extends('admin::layouts.master')
@section('content')

<style>
   .rating .active{color:#ff9705 !important;}
   .card-title .active{color: red;}
</style>


<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Sản phẩm bán chạy</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <strong class="card-title">Quản lý sản phẩm / <a href="admin/warehouse?type=inventory" class="{{Request::get('type') == "inventory" || !Request::get('type') ? "active" : ""}}" >Bán chạy /</a> {{-- <a href="admin/warehouse?type=pay" class="{{Request::get('type') == "pay" ? "active" : ""}}"> Bán Chạy</a> --}}</strong>
            </div>
            {{-- <div class="card-header">
               <strong class="card-title">Quản lý sản phẩm / <a href="admin/warehouse?type=inventory" class="{{Request::get('type') == "inventory" || !Request::get('type') ? "active" : ""}}" >Hàng tồn /</a> <a href="admin/warehouse?type=pay" class="{{Request::get('type') == "pay" ? "active" : ""}}"> Bán Chạy</a></strong>
            </div> --}}
            <br>

            <div class="col-md-6">
                        <form class="form-inline" action="" style="margin-bottom: 20px;">
                           <div class="form-group" style="margin: 0 1% 0 1%;">
                              <strong> Từ </strong>
                           </div>
                           <div class="form-group">       
                              <input type="date" class="form-control" name="from_date" value="{{ request()->get('from_date') }}">
                           </div>
                           <div class="form-group" style="margin: 0 1% 0 1%;">
                              <strong> - </strong>
                           </div>
                           <div class="form-group">       
                              <input type="date" class="form-control" name="to_date" value="{{ request()->get('to_date') }}">
                           </div>
                           <button type="submit" style="margin-left: 1%;"  class="btn btn-info"><i class="fa fa-search"></i></button>
                        </form>
                     </div>



            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Tên sản phẩm</th>
                        <th>Giá - Giá khuyến mãi</th>
                        <th>Loại sản phẩm</th>
                        <th>Hình ảnh</th>
                        <th>Thời gian</th>
                        {{-- <th>Trạng thái</th>
                        <th>Nổi bật</th>
                        <th>Thao tác</th> --}}
                     </tr>
                  </thead>
                  <tbody>
                    
                     @if(isset($products))
                     @foreach($products as $key => $product)
                     
                     <?php 
                        $age =0;
                        if($product->pro_total_rating)
                        {
                           $age = round($product->pro_total_number/$product->pro_total_rating,2);
                        }
                     ?>
                <!--Tính % giá khuyến mãi-->
                     <?php 
                      $price = $product->pro_price;
                      
                         if($product->pro_sale)
                         {
                             $price = $price *(100 - $product->pro_sale)/100;
                      }
                    ?>

                     <tr>
                        <td>{{$key+1}}</td>
                        <td>
                           {{$product -> pro_name}}
                           <ul style="padding-left: 17px;">
                              {{-- <li><span><i class="fas fa-dollar-sign" style="padding-right: 3px;"></i></span><span>{{$product -> pro_price}}(đ)</span></li>
                              <li><span><i class="fas fa-dollar-sign" style="padding-right: 3px;"></i></span><span>{{$product -> pro_sale}}(%)</span></li> --}}

                              <li><span>Đánh giá:</span>
                                 <span class="rating">
                                    @for($i=1;$i<=5;$i++)
                                          <i class="fa fa-star {{$i <= $age ?'active':''}}" style="color: #999;"></i>
                                    @endfor
                                 </span>
                              </li>
                              <li><span>Số lượng:</span><span>{{$product->pro_number}}</span></li>
                              <li><span>Số lượng bán:</span><span>{{$product->pro_pay}}</span></li>
                           </ul>
                        </td>
                      @if($product->pro_sale == 0)
                        <td>{{number_format($product -> pro_price,0,',','.')}}(đ) - {{number_format($product -> pro_sale,0,',','.')}}(%)</td>
                      @else
                        <td>{{number_format($product -> pro_price,0,',','.')}}(đ) - {{number_format($product -> pro_sale,0,',','.')}}(%) = {{number_format($price,0,',','.')}}(đ)</td>
                      @endif
                        <td>{{ isset($product->category->c_name) ? $product->category->c_name : '[N\A]'}}</td>
                        <td>
                           <img src="{{asset("")}}/{{ pare_url_file($product->pro_avatar)}}" alt="ảnh" class="img img-reponsive" style="height: 100px;width: 100px;">
                        </td>
                        <td>{{$product->created_at}}</td>
                      
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .animated -->
@endsection

