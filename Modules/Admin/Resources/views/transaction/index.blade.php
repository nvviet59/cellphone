@extends('admin::layouts.master')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Đơn hàng</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Quản lý đơn hàng</strong>

                        </div>
                        <div class="card-body">
                
               <div class="row">
                     <div class="col-md-6">
                        <form class="form-inline" action="" style="margin-bottom: 20px;">
                           <div class="form-group" style="margin: 0 1% 0 1%;">
                              <strong> Từ </strong>
                           </div>
                           <div class="form-group">       
                              <input type="date" class="form-control" name="from_date" value="{{ request()->get('from_date') }}">
                           </div>
                           <div class="form-group" style="margin: 0 1% 0 1%;">
                              <strong> - </strong>
                           </div>
                           <div class="form-group">       
                              <input type="date" class="form-control" name="to_date" value="{{ request()->get('to_date') }}">
                           </div>
                           <button type="submit" style="margin-left: 1%;"  class="btn btn-info"><i class="fa fa-search"></i></button>
                        </form>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group" style="margin-bottom: 20px; margin-top: 10px;">
                           <p><strong>Tổng tiền:</strong> {{number_format($moneyTotal,0,',','.')}} VNĐ</p>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group" style="margin-bottom: 20px; margin-top: 10px;">
                           <p><strong>Tổng doanh thu:</strong> {{number_format($Total,0,',','.')}} VNĐ</p>
                        </div>
                     </div>
                  </div>
                  <hr>
               <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Thông tin đơn hàng</th>
                        <th>Tổng tiền</th>
                        <th>Trạng thái</th>
                        <th>Thanh toán</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($transactions as $key => $transaction)
                     <tr>
                        <td>{{ $key+1 }}</td>
                        <td>
                           <ul style="padding-left: 17px;">
                              <li><strong>Khách hàng: </strong>{{isset($transaction->user->name) ? $transaction->user->name : '[N\A]'}}</li>
                              <li><strong>Địa chỉ: </strong>{{$transaction->tr_address}}</li>
                              <li><strong>Điện thoại: </strong>{{$transaction->tr_phone}}</li>
                              <li><strong>Thời gian: </strong>{{$transaction->created_at->format('d-m-Y')}}</li>
                           </ul>
                        </td>
                        <td>{{number_format($transaction->tr_total,0,',','.')}} VNĐ</td>
                        <td>
                           @if($transaction->tr_status == 1)
                           <a style="color: white;" class="badge badge-success">Đã xử lý</a>
                           @elseif($transaction->tr_status == 0)
                           <a href="{{route('admin.get.active.transaction',$transaction->id)}}" style="color: white;" class="badge badge-secondary">Chờ xử lý</a>
                           @elseif($transaction->tr_status == 2)
                           <a style="color: white;" class="badge badge-warning">Đang gửi</a>
                           @else
                           <a style="color: white;" class="badge badge-danger">Cancel</a>
                           @endif
                           {{-- @if($transaction->tr_status == 1)
                           <a href="#" class="badge badge-success">Đã xử lý</a>
                           @else
                           <a href="{{route('admin.get.active.transaction',$transaction->id)}}" class="badge badge-secondary">Chờ xử lý</a>
                           @endif --}}

                        </td>
                        <td>
                           @if($transaction->tr_type == 2)
                           <a style="color: white;" class="badge badge-success">Online</a>
                           @else
                           <a style="color:white;" class="badge badge-info">Thường</a>
                           @endif
                        </td>
                        <td>
                           <a class="badge badge-dark" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.transaction',$transaction->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a class="badge badge-danger" onclick="return confirm('Bạn có muốn xóa tin này?')" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.action.transaction',['delete',$transaction->id])}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                           <button class="badge badge-info js_order_item" data-toggle="modal" data-target ="#myModalOrder" data-id="{{$transaction->id}}" style="padding:5px 10px;border:1px solid #eee;" data-href="{{route('admin.get.view.order',$transaction->id)}}"><i class="fas fa-eye"></i> Xem</button>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
         <div class="row">
            <div class="pull-right offset-5">
               {{ $transactions->appends($_GET)->links() }}
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .animated -->
<!-- Modal -->
<div class="modal hide fade" id="myModalOrder" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Chi tiết đơn hàng #<b class="transaction_id"></b></h4>
         </div>
         <div class="modal-body" id="md_content">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
@stop
@section('js')
<script>
   $(document).ready(function() {
     $('#myModalOrder').on('show.bs.modal', function(e) {
       let url = $(e.relatedTarget).data('href');
       let transaction_id = $(e.relatedTarget).data('id');
   
       $.ajax({
         url : url,
         dataType : 'JSON',
         type : 'GET',
         data : {
           id : transaction_id
         },
         success : function(result) {
           console.log(result)
           $("#md_content").html(result);
         }
       });
     })
   });
   
</script>
@endsection