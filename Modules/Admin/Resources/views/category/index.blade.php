@extends('admin::layouts.master')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh mục</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


	 <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Quản lý danh mục </strong>

                        </div>
                        <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Tên danh mục</th>
                        <th>Thứ tự</th>
                        <th>Title Seo</th>
                        <th>Trạng thái</th>
                        <th>Nổi bật</th>
                        {{-- <th>Mô tả</th> --}}
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(isset($categories))
                      @foreach($categories as $key => $category)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$category -> c_name}}</td>
                        <th>{{$category -> thutu}}</th>
                        <td>{{$category -> c_title_seo}}</td>
                        <td>
                          <a href="{{route('admin.get.action.category',['active',$category->id])}}" class="badge {{$category->getStatus($category->a_active)['class']}}">{{$category ->getStatus($category->a_active)['name']}}</a>
                        </td>
                        <td>
                          <a href="{{route('admin.get.action.category',['hot',$category->id])}}" class="badge {{$category->getHome($category->a_home)['class']}}">{{$category ->getHome($category->a_home)['name']}}</a>
                        </td>
                        {{-- <td>{{$category -> c_description_seo}}</td> --}}
                        <td>
                         <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.category',$category->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a class="badge badge-danger" onclick="return confirm('Bạn có muốn xóa tin này?')" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.action.category',['delete',$category->id])}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                      @endif
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
@endsection