@extends('admin::layouts.master')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pages</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card card-info">
            <div class="card-header">
               <strong class="card-title">Quản lý Pages {{-- <a href="{{route('admin.get.create.page_static')}}" class="pull-right badge badge-primary"><i class="fas fa-plus-circle"></i> Thêm mới</a> --}}</strong>
            </div>
            <div class="card-body">
               <table class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th style="width: 20%;">Tên bài viết</th>
                        <th>Thời gian tạo</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <tbody>
                  <?php $i=1; ?>
                     @if(isset($pages))
                     @foreach($pages as $page)
                     <tr>
                        <td>{{$i++}}</td>
                        <td>{{$page -> ps_name}}
                        <td>{{$page->created_at}}</td>
                        <td>
                           <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.page_static',$page->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a onclick="return confirm('Bạn có muốn xóa')" class="badge badge-danger" style="padding:5px 10px;border:1px solid #eee;" href="{{route('deletePage',$page->id)}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                     </tr>
                     @endforeach
                     @endif
                  <?php $i++ ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .animated -->
@endsection