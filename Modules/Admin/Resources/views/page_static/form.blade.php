<form action="" method="post" enctype="multipart/form-data">
   @csrf
   <div class="row">
      <div class="offset-2 col-md-8">
         <div class="form-group">
            <label for="name">Tên bài viết:</label>
            <input type="text" class="form-control" placeholder="Tên bài viết" value="{{ old('ps_name',isset($page->ps_name) ? $page->ps_name : '')}}" required="bạn chưa nhập trường này" name="ps_name">
         </div>
            
         <div class="form-group">
            <label for="name">Chọn trang:</label>
            <select name="type" class="form-control">
               <option value="1" @if($page->ps_type == 1)
               {{"selected"}}
               @endif>Về chúng tôi</option>
               <option value="2"  @if($page->ps_type == 2)
               {{"selected"}} @endif>Thông tin giao hàng</option>
               <option value="3"  @if($page->ps_type == 3)
               {{"selected"}} @endif>Chính sách bảo mật</option>
               <option value="4"  @if($page->ps_type == 4)
               {{"selected"}} @endif>Điều khoản sử dụng</option>

             {{--   <option value="1">Về chúng tôi</option>
               <option value="2">Thông tin giao hàng</option>
               <option value="3">Chính sách bảo mật</option>
               <option value="4">Điều khoản sử dụng</option> --}}
            </select>
         </div>

         <div class="form-group">
            <label for="name">Nội dung:</label>
            <textarea name="ps_content" class="form-control" id="a_content" cols="30" rows="3" required="bạn chưa nhập trường này" placeholder="Nội dung" value="">{{ old('ps_content',isset($page->ps_content) ? $page->ps_content : '')}}</textarea>
         </div>
         <button type="submit" class="btn btn-primary btn-sm">
   <i class="fa fa-dot-circle-o"></i> Lưu thông tin
   </button>
      </div>
      
   </div>
   
</form>
@section('js')
<script>
        CKEDITOR.replace( 'a_content', {
            language:'vi',
            filebrowserBrowseUrl: 'http://localhost/Web_Cellphone/public/ckfinder/ckfinder.html',
            filebrowserUploadUrl: 'http://localhost/Web_Cellphone/public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserWindowWidth: '1000',
            filebrowserWindowHeight: '700'
        } );
    </script>
@endsection