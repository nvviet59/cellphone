

{{-- 

<!DOCTYPE html>
<html style="background-color:#27bda8;">
   <head>
      <title>Admin-Thuận thành tech</title>
      <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
      <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
   </head>
   <style>
      @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
      .login-block{
      background: #DE6262;  /* fallback for old browsers */
      background: -webkit-linear-gradient(to bottom, #FFB88C, #DE6262);  /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to bottom, #51c7a552, #27bda8); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
      float:left;
      width:100%;
      padding : 50px 0;
      }
      .container{background:#fff; border-radius: 10px; box-shadow:15px 20px 0px rgba(0,0,0,0.1);}
      .carousel-inner{border-radius:0 10px 10px 0;}
      .carousel-caption{text-align:left; left:5%;}
      .login-sec{padding: 50px 30px; position:relative;}
      .login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
      .login-sec .copy-text i{color:#FEB58A;}
      .login-sec .copy-text a{color:#E36262;}
      .login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #337ab7;}
      .login-sec h2:after{content:" "; width:100px; height:5px; background:#FEB58A; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
      .btn-login{background: #337ab7; color:#fff; font-weight:600;}
      .banner-text{width:70%; position:absolute; bottom:40px; padding-left:20px;}
      .banner-text h2{color:#fff; font-weight:600;}
      .banner-text h2:after{content:" "; width:100px; height:5px; background:#FFF; display:block; margin-top:20px; border-radius:3px;}
      .banner-text p{color:#fff;}

   </style>
   <style>
              .error-text{
                color:red;
              }
            </style>
   <body>
      <!------ Include the above in your HEAD tag ---------->
      <section class="login-block">
         <div class="container">

  @if (session('thongbao'))
                  <span class="badge badge-pill badge-danger">{{ session('thongbao') }}</span> 
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               @endif
               
            <div class="login-sec" style="margin: auto;
  width: 50%;">
               <h2 class="text-center">Đăng nhập</h2>
               <form class="login-form" action="" method="post">
                  @csrf
                  <div class="form-group">
                     <label for="exampleInputEmail1" class="text-uppercase">Email:</label>
                     <input type="text" class="form-control" name="email" placeholder="example@gmail.com">
                     @if($errors->has('email'))
                          <span class="error-text">
                          {{$errors->first('email')}}
                          </span>
                        @endif
                  </div>
                  <div class="form-group">
                     <label for="exampleInputPassword1" class="text-uppercase">Mật khẩu:</label>
                     <input type="password" class="form-control" name="password" placeholder="nhập mật khẩu">
                     @if($errors->has('password'))
                          <span class="error-text">
                          {{$errors->first('password')}}
                          </span>
                        @endif
                  </div>
                  <div class="form-check">
                     <label class="form-check-label">
                     <input type="checkbox" name="remember" class="form-check-input">
                     <small>Nhớ mật khẩu</small>
                     </label>
                     <button type="submit" class="btn btn-login float-right">Đăng nhập</button>
                  </div>
               </form>
            </div>
            <div class="login-sec" style="margin:auto;padding-top: 0;
  width: 50%; text-align: center;font-size: 13px;">Created with <i class="fa fa-heart"></i> by Thuanthanhtech © 2020 THUANTHANHTECH Việt Nam. Bảo lưu mọi quyền. Website tuân thủ chuẩn 960 gs, XHTML & CSS.</div>
           
      </section>
   </body>
</html>
 --}}



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
    <style>
        body{
            /*background-image: url("../theme_admin1/bglogin.jpg");*/
            background-color: gray;
            background-repeat:  no-repeat;
        }
        .error-text{
          color: red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class=" card card-default" style="margin-top: 200px; background-color: rgba(0, 0, 0, 0.5); color: white; font-family: 'Comic Sans MS'">
                    <div class=" card card-header" STYLE="text-align: center"><span>ĐĂNG NHẬP</span></div>
                     @if (session('thongbao'))
                  <span class="badge badge-pill badge-danger">{{ session('thongbao') }}</span> 
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               @endif
                    <div class="card-body">
                        <form action="" method="post">
                        {{-- <form action="{{route('dangnhap')}}" method="post"> --}}
                            @csrf
                           
                            <div class="form-group">
                                <label for="" >Email</label>
                                <input type="email" class="form-control" placeholder="Email" name="email">
                                 @if($errors->has('email'))
                                  <span class="error-text">
                                  {{$errors->first('email')}}
                                  </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" class="form-control" placeholder="Password" name="password">
                                 @if($errors->has('password'))
                          <span class="error-text">
                          {{$errors->first('password')}}
                          </span>
                        @endif
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-secondary" value="Đăng nhập">
                                <input type="reset" class="btn btn-danger" style="float: right;" value="reset">
                            </div>
                        </form>
                    </div>
                    {{-- <div class="card-footer"><cite>create by Swac</cite></div> --}}
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>



   