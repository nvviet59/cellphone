@extends('admin::layouts.master')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tài khoản</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Tài khoản</a></li>
              <li class="breadcrumb-item active"><a href="{{route('user.list')}}">Danh sách</a></li>
              <li class="breadcrumb-item active">Sửa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card card-warning">
            <div class="card-header">
               <strong class="card-title">Sửa tài khoản</strong>
            </div>
            <div class="card-body">
               <form action="admin/user/sua/{{$users->id}}" method="POST" enctype="multipart/form-data">
               @csrf
               <div class="col-md-12">
                
                @if (session('loi'))
                   <div class="alert  alert-danger alert-dismissible fade show" style="width:350px;" role="alert">
                    <span class="badge badge-pill badge-danger">{{ session('loi') }}</span> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                @endif
                
                  <style>
                    .error-text{
                      color:red;
                    }
                  </style>

                  @if (session('thongbao'))
                    <div class="alert  alert-success alert-dismissible fade show" style="width:250px;" role="alert">
                      <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                  @endif        
          

                <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label>Nhập tên tài khoản(*):</label>
                     <input type="text" name="name" value="{{$users->name}}" required="" class="form-control" placeholder="Nhập tên">
                     @if($errors->has('name'))
                     <span class="error-text">
                     {{$errors->first('name')}}
                     </span>
                     @endif
                  </div>
                  <div class="form-group">
                     <label>Email(*):</label>
                     <input type="text" name="email" required="" value="{{ old('email',isset($users->email) ? $users->email : '')}}" class="form-control" placeholder="Nhập email" >
                     @if($errors->has('email'))
                     <span class="error-text">
                     {{$errors->first('email')}}
                     </span>
                     @endif
                  </div>
                  <div class="form-group">
                     <label>SĐT(*):</label>
                     <input type="text" name="phone" onkeypress="InputNumber(event)" value="{{$users->phone}}" class="form-control" placeholder="Nhập phone">
                     @if($errors->has('phone'))
                     <span class="error-text">
                     {{$errors->first('phone')}}
                     </span>
                     @endif
                  </div>

              

                  
               </div>
               <div class="col-md-6">

                  {{-- <div class="form-group">
                     <label>PostOffice:</label>
                     <input type="text" name="postoffice" class="form-control" value="{{$users->postoffice}}" placeholder="Nhập địa chỉ văn phòng làm việc">
                  </div>
                  <div class="form-group">
                     <label>Địa chỉ:</label>
                     <input type="text" name="address" value="{{$users->address}}" class="form-control" placeholder="Nhập địa chỉ">
                  </div>
                  <div class="form-group">
                     <label>Chứng minh nhân dân:</label>
                     <input type="text" name="cmnd" onkeypress="InputNumber(event)" value="{{$users->cmnd}}" class="form-control" placeholder="Nhập số cmnd">
                     @if($errors->has('cmnd'))
                     <span class="error-text">
                     {{$errors->first('cmnd')}}
                     </span>
                     @endif
                  </div> --}}

                  {{-- <div class="form-group">
                          <label>Avatar:</label>
                          <br>
                          <img src="avatar/{{$users->avatar}}" width="150px;">
                          <input type="file" name="avatar" class="form-control" />
                  </div> --}}
                <div class="form-group">
                          <input type="checkbox" name="changePassword" id="changePassword">
                          <label>Đổi mật khẩu(*):</label>
                          <input type="Password" name="password" class="form-control password" disabled="" />
                           @if($errors->has('password'))
                           <span class="error-text">
                           {{$errors->first('password')}}
                           </span>
                           @endif
                      </div>

                      <div class="form-group">
                          <label>Nhập lại Password(*):</label>
                          <input type="Password" name="passwordAgain" class="form-control password" disabled="" />
                           @if($errors->has('passwordAgain'))
                     <span class="error-text">
                     {{$errors->first('passwordAgain')}}
                     </span>
                     @endif
                      </div>


                 {{--  <div class="form-group">
                          <label>Quyền người dùng:</label>
                          <label class="radio-inline">
                              <input name="active" value="0"
                                @if($users->active == 0)
                                {{"checked"}}
                                @endif
                               type="radio">không hoạt động
                          </label>
                          <label class="radio-inline">
                              <input name="active" value="1"
                               @if($users->active == 1)
                                {{"checked"}}
                                @endif
                               type="radio">Hoạt động
                          </label>
                      </div> --}}
                  
               </div>
             </div>
             </div>
               <button type="submit" class="btn btn-primary btn-sm">Sửa</button>
               {{-- <button type="reset" class="btn btn-danger btn-sm">Làm mới</button> --}}
               <a href="{{route('user.list')}}"><button type="button" class="btn btn-danger btn-sm">Quay lại</button></a>
               <form>
            </div>

         </div>

      </div>
   </div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(document).ready(function(){
    $("#changePassword").change(function(){
      if($(this).is(":checked"))
      {
        $(".password").removeAttr('disabled');
      }else{
        $(".password").attr('disabled','');
      }
    });
  });
</script>
@endsection