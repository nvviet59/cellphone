@extends('admin::layouts.master')
@section('content')

<style>
  .modal-body{
    max-width: 100%;
  }
  .chitiet{
    border: none;
  }
</style>

<div id="updateModal" class="modal fade " role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <form action="" id="updateForm" method="post">
            <div class="modal-header">
              <h4 class="modal-title">Thông tin người dùng</h4>
                <button type="button" class="close btn btn-outline-danger" data-dismiss="modal">&times;</button>
            </div>
            
                @csrf
                <div class="modal-body">
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Thông tin người dùng</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                      <table class="table table-sm">
                      <thead>
                           <tr>
                                <th> ID</th>
                                <th>Tên tài khoản</th>
                                <th>SĐT</th>
                                <th>Email</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                            <td><input  id="id"  class="chitiet"></td>
                            <td><input  id="name"  class="chitiet"></td>
                            <td><input  id="phone" class="chitiet"></td>
                            <td><input  id="email" class="chitiet"></td>
                            {{-- <td><input  id="cmnd" class="chitiet"></td> --}}
                           </tr>
                           
                        </tbody>
                  </table>
                  </div>
                </div>
              </div>
                <div class="modal-footer">
                </div>
            
            </form>
        </div>

    </div>
</div>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tài khoản</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Tài khoản</li>
              <li class="breadcrumb-item active"><a href="{{route('user.list')}}">Danh sách</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

	<div class="animated fadeIn">
                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                                <strong class="card-title">Danh sách tài khoản  <a href="{{route('user.create')}}" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>
                        <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>STT</th>
                        <th>Tên</th>
                        <th>Email</th>
                        {{-- <th>Level</th> --}}
                        <th>Phone</th>
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($users as $key => $u)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>
                          <p>{{$u->name}}</p>
                          <img width="50px" src="avatar/{{$u->avatar}}" alt="avatar" />
                        </td>
                        <td>{{$u->email}}</td>
                      {{--   <td>
                            @if($u->quyen==1)
                            {{"Admin"}}
                            @else
                            {{"Thường"}}
                            @endif
                        </td> --}}
                        <td>{{$u->phone}}</td>
                        <td>
                              <a href="admin/user/sua/{{$u->id}}" class="badge badge-warning" style="padding:5px 10px;color: white;"><i class="fas fa-pen"></i> Sửa</a>
                             {{--  <a class="badge badge-info chuothover" data-toggle="modal" data-target="#updateModal" onclick="loadData({{$u->id}})" style="padding:5px 10px;cursor: pointer;color: white;"><i class="fas fa-edit"></i> Chi tiết</a> --}}
                              <a onclick="return confirm('Bạn có muốn xóa?')" href="{{route('deleteUser',$u->id)}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('js')
    <script type="text/javascript">
        var loadData = (id) =>{
           var url = `{{url('admin/user/show/')}}/${id}`;
           jQuery.ajax({
            url:url,
            method:'GET',
            success: function(response)
            {
              console.log("success");
               jQuery.each(response, function(key, item){
                    console.log(item);
                    $("#id").val(item['id']);
                    $("#name").val(item['name']);
                    $("#phone").val(item['phone']);
                    // $("#cmnd").val(item['cmnd']);
                    $("#email").val(item['email']);
                    
                  });
                  
                  // $("#updateForm").attr("action", url);
            },
            error: function(response)
            {
              console.log('error');
              console.log(response);
            }
           });
        }  
    </script>
    @endsection