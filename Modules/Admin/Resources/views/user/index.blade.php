@extends('admin::layouts.master')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Thành viên</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
	 <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Quản lý thành viên</strong>
                        </div>
                        <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Tên hiển thị</th>
                        <th>Email</th>
                        <th>Số ĐT</th>
                        <th>Hình ảnh</th>
                        {{-- <th>Thao tác</th> --}}
                      </tr>
                    </thead>
                    
                    <tbody>
                    <?php $i=1; ?>
                     @if(isset($users))
                        @foreach($users as $user)
                            <tr>
                        <td>{{$i++}}</td>
                        <td>{{$user-> name}}</td>
                        <td>{{$user -> email}}</td>
                        <td>{{$user -> phone}}</td>
                        <td>
                           <img src="{{asset("")}}/{{ pare_url_file($user->avatar)}}" alt="ảnh" class="img img-reponsive" style="height: 100px;width: 100px;">
                        </td>
                        {{-- <td>
                           <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.product',$user->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a class="badge badge-danger" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.action.product',['delete',$user->id])}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td> --}}
                     </tr>
                        @endforeach
                     @endif

                     <?php $i++ ?>
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>
                </div>
            </div><!-- .animated -->
@endsection