@extends('admin::layouts.master')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Bài viết</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


    <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Quản lý Bài viết {{-- <a href="{{route('admin.get.create.article')}}" class="pull-right badge badge-primary"><i class="fas fa-plus-circle"></i> Thêm mới</a> --}}</strong>

                        </div>
                        <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th style="width: 20%;">Tên bài viết</th>
                        <th style="width: 150px;">Hình ảnh</th>
                        <th style="width: 300px;">Mô tả</th>
                        <th>Trạng thái</th>
                        <th>Hot</th>
                        <th>Ngày tạo</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($articles))
                     @foreach($articles as $article)
                     <tr>
                        <td>{{$article -> id}}</td>
                        <td>
                           {{$article -> a_name}}
                        </td>
                        <td>
                           <img src="{{asset("")}}/{{ pare_url_file($article->a_avatar)}}" alt="ảnh" class="img img-reponsive" style="height: 100px;width: 100px;">
                        </td>
                        <td>{{$article -> a_description}}</td>
                        <td>
                           <a href="{{route('admin.get.action.article',['active',$article->id])}}" class="badge {{$article->getStatus($article->a_active)['class']}}">{{$article ->getStatus($article->a_active)['name']}}</a>
                        </td>
                        <td>
                           <a href="{{route('admin.get.action.article',['hot',$article->id])}}" class="badge {{$article->getHot($article->a_active)['class']}}">{{$article ->getHot($article->a_active)['name']}}</a>
                        </td>
                        <td>
                           {{$article->created_at}}
                        </td>
                        <td>
                           <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.article',$article->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a class="badge badge-danger" onclick="return confirm('Bạn có muốn xóa tin này?')" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.action.article',['delete',$article->id])}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .animated -->
@endsection