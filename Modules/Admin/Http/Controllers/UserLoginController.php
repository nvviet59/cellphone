<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests\RequestUserLogin;
use Validator;
use App\Models\Admin;

class UserLoginController extends Controller
{
   
    public function index()
    {
    	$users = Admin::all();
    	  return view('admin::userlogin.danhsach',['users'=>$users]);
    }

    public function create()
    {
    	return view('admin::userlogin.them');
    }

    public function store(RequestUserLogin $requestUserLogin)
    {
      // $this->
    	 // $this->validate($request,
      //       [
      //           'name'=>'required|min:3',
      //           'email'=>'required|email|unique:admins,email',
      //           'password'=>'required|min:3|max:33',
      //           'passwordAgain'=>'required|same:password'
      //       ],
      //       [
      //           'name.required'=>'Bạn chưa nhập tên',
      //           'name.min'=>'Tên người dùng phải có ít nhất 3 ký tự',
      //           'email.required'=>'Bạn chưa nhập Email',
      //           'email.email'=>'Bạn chưa nhập đúng định dạng email',
      //           'email.unique'=>'Email đã tồn tại',
      //           'password.required'=>'Bạn chưa nhập password',
      //           'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
      //           'password.max'=>'Mật khẩu có tối đa là 33 ký tự',
      //           'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
      //           'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp'
      //       ]);

            $users = new Admin;
            $users->name = $requestUserLogin->name;
            $users->email = $requestUserLogin->email;
            $users->phone = $requestUserLogin->phone;
            $users->password = bcrypt($requestUserLogin->password);
            // $users->active = $requestUserLogin->active;

            // dd($users);
            $users->save();

            return back()->with('thongbao','Thêm tài khoản thành công');
    }

    public function edit($id)
    {
    	$users = Admin::find($id);

    	return view('admin::userlogin.sua',['users'=>$users]);
    }

    public function update(Request $request,$id)
    {
    	$users = Admin::find($id);

    	  $users->name = $request->name;
        $users->email = $request->email;
        $users->phone = $request->phone;
        // $users->active = $request->active;

         if($request->changePassword == "on")
            {
              $request->validate([
                'password'=>'required|min:3|max:33',
                'passwordAgain'=>'required|same:password'
              ],
              [
                'password.required'=>'Bạn chưa nhập password',
                'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
                'password.max'=>'Mật khẩu có tối đa là 33 ký tự',
                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp'
              ]);
              
              $users->password = bcrypt($request->password);
            }


        // dd($users);
        $users->save();

        return back()->with('thongbao','Sửa tài khoản thành công');

    }

    public function getXoa($id)
    {
    	Admin::where('id',$id)->delete();

    	return redirect()->back();
    }
   
}
