<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\PageStatic;

class AdminPageStaticCotrllerController extends Controller
{
    public function index()
    {
        $pages = PageStatic::all();
        return view('admin::page_static.index',compact('pages'));
    }

    public function create()
    {
        return view('admin::page_static.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->CreateOrUpdate($request);
        return redirect()->back();
    }

    public function edit($id)
    {
        $page = PageStatic::find($id);
        return view('admin::page_static.create',compact('page'));
    }
    public function update(Request $request, $id)
    {
        $this->CreateOrUpdate($request,$id);
       return redirect()->back();
    }

    public function CreateOrUpdate($request,$id='')
    {
        $page = new PageStatic();

        if($id) $page = PageStatic::find($id);

       $page->ps_name = $request->ps_name;
       $page->ps_content = $request->ps_content;
       $page->ps_type = $request->type;

       $page->save();

       return redirect()->back();
    }

    public function delete($id)
    {
        PageStatic::where('id',$id)->delete();
        return redirect()->back();
    }
}
