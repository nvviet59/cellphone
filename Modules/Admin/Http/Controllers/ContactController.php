<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Contact;

class ContactController extends Controller
{
  public function index()
    {
        $contact = Contact::all();
        return view('admin::contact.index',['contact'=>$contact]);
    }

    //Xóa Contact
    public function action($getXoa,$id)
    {
        if($getXoa)
        {
            $contact = Contact::find($id);
            switch($getXoa)
            {
                case 'delete':
                    $contact->delete();
                    break;

                case 'active':
                     $contact->c_status = $contact->c_status ? 0:1;
                     $contact->save();
                    break;
            }
        }
        return redirect()->back();
    }
}
