<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Requests\RequestProduct;
use App\Models\Category;
use App\Models\Product;
use App\Models\Parameter; 
use App\Models\ProductImages; 
use App\Exports\ProductExport;  
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use DB;

class AdminProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::with('category:id,c_name');

        if($request->name) $products->where('pro_name','like','%'.$request->name.'%');
        
        if($request->cate) $products->where('pro_category_id',$request->cate);
        $products =$products->orderByDesc('id')->paginate(50);
        
        $categories = $this->getCategories();
        $para = $this->getParameter();

        $viewData = [
            'products' =>$products,
            'categories'=> $categories,
            'para' => $para
        ];
        return view('admin::product.index',$viewData);
    }

    //Excel export
     public function export() 
    {
        return Excel::download(new ProductExport, 'export.xlsx');
    }

    public function create()
    {
        $categories = $this->getCategories();
        $para = $this->getParameter();
        // $product_imge = ProductImages::all();
        return view('admin::product.create',compact('categories','para'));
    }

    public function store(RequestProduct $requestProduct)
    {
        // dd($requestProduct->all());
         $this->insertOrUpdate($requestProduct);
         return redirect()->back()->with('thongbao','Thêm thành công');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        // $product_img = ProductImages::with('image_pro')->find($id);
        $product_imge = ProductImages::where('product_id',$product->id)->get();
        $categories = $this->getCategories();
        $para = $this->getParameter();
        return view('admin::product.update',compact('product','categories','para','product_imge'));
    }

    public function update(RequestProduct $requestProduct,$id)
    {
        $this->insertOrUpdate($requestProduct,$id);
        return redirect()->back()->with('thongbao','Sửa thành công');
    }

    public function getCategories()
    {
        return Category::all();
    }
    public function getParameter()
    {
        return Parameter::all();
    }

    public function insertOrUpdate($requestProduct,$id='')
    {
        // dd($requestProduct->value, json_encode($requestProduct->value), $requestProduct->key ,json_encode($requestProduct->key));
        $product = new Product();

        if($id) $product = Product::find($id);

        $product->pro_name = $requestProduct->pro_name;
        $product->key_param = json_encode($requestProduct->key_param);
        $product->value_param = json_encode($requestProduct->value_param);
        $product->pro_slug = str_slug($requestProduct->pro_name);
        $product->pro_category_id = $requestProduct->pro_category_id;
        $product->pro_price = $requestProduct->pro_price;
        $product->pro_sale = $requestProduct->pro_sale;
        $product->pro_number = $requestProduct->pro_number;
        $product->pro_content = $requestProduct->pro_content;
        $product->pro_parameter = $requestProduct->para_name;
        $product->pro_hot = $requestProduct->pro_hot;
        $product->pro_description = $requestProduct->pro_description;
        $product->pro_title_seo = $requestProduct->pro_title_seo ? $requestProduct->pro_title_seo : $requestProduct->pro_name;
        $product->pro_description_seo = $requestProduct->pro_description_seo ? $requestProduct->pro_description_seo : $requestProduct->pro_description_seo;

        
        if($requestProduct->hasFile('avatar'))
        {
            $file = upload_image('avatar');

            if(isset($file['name']))
            {
                $product->pro_avatar = $file['name'];
            }
        }

        $product->save();

        $picture = $requestProduct->imageDetail;
        $product_id = $product->id;
        if(!empty($picture))
        {
            foreach($picture as $file){

                $product_img = new ProductImages;
                // if($id) $product_img = ProductImages::find($id);
                // dd($product_img);

                if(isset($file)){
                    $product_img->image = $file->getClientOriginalName();
                    $product_img->product_id = $product_id;
                    $file->move('uploads/detail/',$file->getClientOriginalName());

                    $product_img->save();
                }
            }
        }

    }
    public function action($action,$id)
    {
        if($action)
        {
            $product = Product::find($id);
            switch($action)
            {
                case 'delete':
                    $product->delete();
                    break;
                case 'active':
                    $product->pro_active = $product->pro_active ? 0:1;
                     $product->save();
                    break;

                 case 'hot':
                    $product->pro_hot = $product->pro_hot ? 0:1;
                     $product->save();
                    break;    
            }
           
        }
        return redirect()->back();
    }

    public function getXoa($id)
    {
        $product_img = ProductImages::where('id',$id)->first();
        if (file_exists('uploads/detail/'.$product_img->image)) {
                unlink('uploads/detail/'.$product_img->image);
            }
            $product_img->delete();    
        
        return response()->json();
        
    }

    public function delete(Request $request)
    {
        $delid = $request->input('delid');
        Product::where('id',$delid)->delete();
        return redirect()->back();
    }
}
