<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;

class AdminAuthController extends Controller
{
   
    public function getLogin()
    {
        return view('admin::auth.login');
    }

    public function postLogin(Request $request)
    {
        $data = $request->only('email', 'password');

        
        if (Auth::guard('admins')->attempt($data)) {
            // if(Auth::guard('admins')->active ===1)
            //     return redirect()->route('admin.home');
            // else if(Auth::guard('admins')->active ===0)
            //     return redirect()->route('admin.logout');
          return redirect()->route('admin.home');
        }
        return redirect()->back();
    }
    public function getLogout()
    {
        Auth::guard('admins')->logout();
        return redirect()->route('admin.login');
    }
}
