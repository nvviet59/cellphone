<?php

namespace App\Exports;

// use Maatwebsite\Excel\Concerns\FromCollection;
// use App\Models\Product;

// class ProductExport implements FromCollection
// {
   
//     public function collection()
//     {
//         //
//         return Product::all();
//     }
// }
use App\Models\Product;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProductExport implements FromView
{
    public function view(): View
    {
        return view('admin::product.table', [
            'products' => Product::all()
        ]);
    }
}