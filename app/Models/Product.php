<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\User;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    //
    protected $table ='products';
    protected $guarded = [];
    protected $fillable = [
        'pro_author_id',
];
	const STATUS_PUBLIC =1;
    const STATUS_PRIVATE =0;

    const HOT_ON =1;
    const HOT_OFF =0;

    protected $status=[
    	1=>[
    		'name' => 'Public',
    		'class' => 'badge-danger'
    	],
    	0=>[
    		'name' => 'Private',
    		'class' => 'badge-secondary'
    	]
    ];
     protected $hot=[
        1=>[
            'name' => 'Nổi bật',
            'class' => 'badge-success'
        ],
        0=>[
            'name' => 'Không',
            'class' => 'badge-secondary'
        ]
    ];

    public function getStatus()
    {
    	return array_get($this->status,$this->pro_active,'[N\A]');
    }
    public function getHot()
    {
        return array_get($this->hot,$this->pro_hot,'[N\A]');
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'pro_category_id');
    }

    public function image_pro()
    {
        return $this->hasMany('App\Models\ProductImages');
    }

    public static function filter($param = null) {
        $products = new Product();

        if (!empty($param['from_date']) && empty($param['to_date'])) {
            $products = $products->where('products.created_at', '>=', $param['from_date']);
        }

        if (empty($param['from_date']) && !empty($param['to_date'])) {
            $products = $products->where('products.created_at', '<=', $param['to_date']);
        }

        if (!empty($param['from_date']) && !empty($param['to_date'])) {
            $products = $products->whereBetween('products.created_at', [$param['from_date'], date('Y-m-d', strtotime("+1 day", strtotime($param['to_date'])))]);
        }

        return $products;
    }


// cái hàm nay để thay cho 2 hàm bên controller 
    public static function getDataProduct($param = null) {
        $products = self::filter($param)->orderBy('pro_pay','DESC');
        $products->select('products.*')->where('pro_pay','>=',3)->paginate(4);
        return $products;
    }
}
