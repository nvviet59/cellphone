<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table ='articles';

    const HOT =1;

     protected $status=[
    	1=>[
    		'name' => 'Public',
    		'class' => 'badge-danger'
    	],
    	0=>[
    		'name' => 'Private',
    		'class' => 'badge-secondary'
    	]
    ];
    protected $hot=[
       1=>[
            'name' => 'Nổi bật',
            'class' => 'badge-primary'
        ],
        0=>[
            'name' => 'Không',
            'class' => 'badge-secondary'
        ]
    ];

    public function getHot()
    {
        return array_get($this->hot,$this->a_hot,'[N\A]');
    }

    public function getStatus()
    {
    	return array_get($this->status,$this->a_active,'[N\A]');
    }
}
