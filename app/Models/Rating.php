<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Rating extends Model
{
    //
    protected $table = 'ratings';


    public function user()
    {
    	return $this->belongSto(User::class,'ra_user_id');
    }

    public function product()
    {
    	return $this->belongSto(Product::class,'ra_product_id');
    }
}
