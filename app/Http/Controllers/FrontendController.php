<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Category;
use App\Models\Category;
// use Models\Product;
// use Illuminate\View\View;
use DB;
use Illuminate\Support\Facades\View;

class FrontendController extends Controller
{
    public function __construct()
    {
    	$categories = Category::all();
    	 View::share('categories', $categories);
    }
}
