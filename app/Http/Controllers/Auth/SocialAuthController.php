<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\SocialAccountService;
use Socialite;

class SocialAuthController extends Controller
{
    //
     public function redirect($social)
    {
        // dd($social);
        return Socialite::driver($social)->redirect();
    }

    public function callback($social)
    {
        $user = SocialAccountService::createOrGetUser(Socialite::driver($social)->stateless()->user(), $social);
        
        if(
        \Auth::attempt([
            'email' => $user->email,
            'password' => $user->name,
        ])){
              return redirect()->route('user.dashboard');
        }
// dd($user);
        return redirect()->to('/');
    }
}
