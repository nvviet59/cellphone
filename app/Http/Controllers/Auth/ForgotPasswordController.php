<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Requests\RequestResetPassword;
use App\User;
use Carbon\Carbon;
use Mail;

class ForgotPasswordController extends Controller
{
    public function getFormResetPassword()
    {
        return view('auth.passwords.email');
    }

    public function sendCodeResetPassword(Request $request)
    {
        $email = $request->email;   
        $checkUser = User::where('email',$email)->first();

        if(!$checkUser)
        {
            return redirect()->back()->with('thongbao','Email không tồn tại');
        }

       $code = bcrypt(md5(time().$email));

       $checkUser->code = $code;
       $checkUser->time_code = Carbon::now();
       $checkUser->save();
       $url = route('get.link.reset.password',['code'=>$checkUser->code,'email'=>$email]);

       $data = [
        'route'=>$url
       ];

       Mail::send('email.reset_password',$data, function($message) use ($email){
            $message->to($email, 'Visitor')->subject('Lấy lại mật khẩu');
        });

       return redirect()->back()->with('thongbao','Đã gửi về Email của bạn');
    }

    public function resetPassword(Request $request)
    {
        $code = $request->code;
        $email = $request->email;

        $checkUser = User::where([
            'code'=>$code,
            'email' =>$email
        ])
        ->first();
        if(!$checkUser)
        {
            return redirect('/')->with('warning','Link bị lỗi, vui lòng thử lại sau');
        }

        return view('auth.passwords.reset');
    }

    public function saveResetPassword(RequestResetPassword $requestResetPassword)
    {
        if($requestResetPassword->password)
        {
            $code = $requestResetPassword->code;
            $email = $requestResetPassword->email;

        $checkUser = User::where([
            'code'=>$code,
            'email' =>$email
        ])
        ->first();
        }

        if(!$checkUser)
        {
            return redirect('/')->with('warning','Link bị lỗi, vui lòng thử lại sau');
        }
        $checkUser->password = bcrypt($requestResetPassword->password);
        $checkUser->save();

        return redirect()->route('get.login')->with('thongbao','Đổi mật khẩu thành công');
    }
}
